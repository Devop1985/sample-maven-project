pipeline {
  agent {
    kubernetes {
      label 'jdk8'
      yamlFile 'build-pod.yml'
      defaultContainer 'jdk8'
      idleMinutes 30
        }
    }

    options {
      gitLabConnection('Gitlab')
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }

  stages {
    stage('Clean') {
      steps {
        sh "chmod 755 mvnw"
        sh "./mvnw clean"
            }
        }

    stage('Build') {
      steps {
        sh "./mvnw -B compile -DskipTests"
            }
        }

    stage('Test') {
      steps {
        sh "./mvnw -B verify"
            }
        }

    stage('Package') {
      steps {
        sh "./mvnw -B package"
            }
        }

    stage('Deploy') {

      steps {
          withCredentials([file(credentialsId: 'maven_settings', variable: 'MAVEN_SETTINGS')
                ]) {
            sh "./mvnw -B -s $MAVEN_SETTINGS deploy"
                }
            }
        }

            stage('Code quality') {

              steps {
                  withCredentials([file(credentialsId: 'maven_settings', variable: 'MAVEN_SETTINGS')
                        ]) {
                    sh "./mvnw sonar:sonar"
                        }
                    }
                }
    }

  post {
    always {
      archiveArtifacts artifacts: 'target/*.jar', fingerprint: true
      junit "target/surefire-reports/*.xml"
    }

    failure {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }
}
